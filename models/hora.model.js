const mongoose = require("mongoose");


const schema = mongoose.Schema({
	fecha: Date,
    horastrabajadas: Number,
    proyecto: String,
    actividades: String
});

module.exports = mongoose.model("Tarea", schema);