const express = require('express');
const mongoose  = require('mongoose');
const app = express();
const cors = require('cors');
const port = 3000;


mongoose.connect("mongodb://localhost:27017/horascoxti", { useNewUrlParser: true })
	.then(() => {
		app.listen(port, () => {
			console.log("Server and Database has started!");
            console.log(`app running on port ${port}!`);
		});
	});

	app.use(express.urlencoded({extended:false}));
    app.use(express.json());
	app.use('/', require('./routes/routes'));
	app.get('/', (req,res)=>{
		res.set('Access-Control-Allow-Origin', 'http://localhost:3000');
	});
	app.use(cors());
	app.use(function(req, res, next) {
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
		res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
		res.setHeader('Access-Control-Allow-Credentials', true);
		next();
	});

