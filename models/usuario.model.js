const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true,
        minlenght: 5,
    },
});

const user = mongoose.model("Usuario", UserSchema);
module.exports = user;


