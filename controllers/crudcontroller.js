// const mongoose = require("mongoose");
'use strict';
const Tarea = require('../models/hora.model');

class Controller{
static async list(req,res){
    try {
    const tasks = await Tarea.find();
    res.send(tasks)
    } catch (error) {
        res.status(500).render('errors/500', { error } );
    }
}

static async listById(req, res, next){
    const id  = req.params.id;
    Tarea.findById(id)
    .then(data =>{
        if (!data) {
            res.status(404).send({message: 'Task Not Found'});
        }else{
            res.send(data);
        }
    }).catch(err =>{
        res.status(500).send({message: 'Error task 500' + err});
    })
    
}

static async save(req,res){
    const task = new Tarea({
        fecha: req.body.fecha,
        horastrabajadas: req.body.horastrabajadas,
        proyecto: req.body.proyecto,
        actividades: req.body.actividades
    });
    await task.save();
    res.send(task);
};

static async edit(req,res){
   try {
    const _id = await Tarea.findByIdAndUpdate(req.body._id, req.body);
    res.json({status: 'task updated succesfully', _id});
   } catch (error) {
     res.status(500).json({error: error.message});
   }
};

static async delete(req,res){
    const _id  = req.body._id;
    await Tarea.findByIdAndDelete(_id);
    res.json({
      res: "tarea eliminada"
    });
};

}

module.exports = Controller


