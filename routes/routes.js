const express = require('express');
const app = express();
const cors = require('cors');
const router = express.Router();
const Controller = require('../controllers/crudcontroller');
const usuariocontroller = require('../controllers/usuariocontroller');

//login and register
router.post('/register', usuariocontroller.register);

router.post('/login', usuariocontroller.login);

//Get all tasks work
router.get("/tareas", Controller.list);

router.get('/tareas/:id', Controller.listById);

//create tasks
router.post('/tareas/create', Controller.save);

//update hours
router.patch('/tareas/update', Controller.edit);

//delete hours works
router.delete('/tareas/delete', Controller.delete);

app.use(cors());

module.exports = router


